'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

gulp.task('styles', function () {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('build/css'));
});

gulp.task('html', function () {
   return gulp.src('src/index.html')
       .pipe(gulp.dest('build/'));
});

gulp.task('watch', function () {
    gulp.watch('src/**/*.*', gulp.series('styles', 'html'));
});

// // ======= //
//
// gulp.task('style', function (done) {
//     return gulp.src('scss/*.scss')
//         .pipe(sass().on('error', sass.logError))
//         .pipe(cleanCSS())
//         .pipe(gulp.dest('build/css'));
//
//     done();
// });
//
// gulp.task('style:watch', function () {
//     return gulp
//         .watch(
//             'scss/*.scss',
//             gulp.series('style')
//         );
// });
//
