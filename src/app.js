// load more products
$(function () {
  $(".furniture-gallery .card").slice(0, 3).show();
  $("#productsLoadMore").on('click', function (e) {
    e.preventDefault();
    $("div.furniture-gallery .card:hidden").slice(0, 3).slideDown();
    if ($("div.furniture-gallery .card:hidden").length == 0) {
      $("#productsLoadMore").fadeOut('slow');
    }
    $('div.furniture-gallery').animate({
      scrollTop: $(this).offset().top
    }, 1500);
  });
});




// filter category hover
$(document).ready(function(){
  $(".cat-item").click(function(){
    $(this).toggleClass("filter_active");
  });
});

// filter tags hover
$(document).ready(function(){
  $(".ftag").click(function(){
     $(this).toggleClass("filter_active");
  });
});